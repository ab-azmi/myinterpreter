import basic
import bcolors


while True:
    text = input(f'{bcolors.bcolors.WARNING}|>{bcolors.bcolors.ENDC}{bcolors.bcolors.OKBLUE})^({bcolors.bcolors.ENDC}{bcolors.bcolors.WARNING}<|{bcolors.bcolors.ENDC}{bcolors.bcolors.OKGREEN}->{bcolors.bcolors.ENDC}')

    result, error = basic.run('<stdin>', text)

    if error: print(error.as_string())
    else: print(result)
